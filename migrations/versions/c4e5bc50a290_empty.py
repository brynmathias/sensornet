"""empty

Revision ID: c4e5bc50a290
Revises: 
Create Date: 2023-04-08 13:11:26.446437

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "c4e5bc50a290"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
