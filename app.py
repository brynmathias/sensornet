from fastapi import FastAPI
from fastapi.responses import JSONResponse

from sensornet.api.main import router as api_router
from sensornet.api.db import async_engine
from sensornet.db_model.base import Base
import asyncio
from fastapi.middleware.cors import CORSMiddleware


# Add CORS middleware

app = FastAPI(title="sensornet")
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # You can specify the allowed origins, for example: ["http://localhost:3000"]
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(api_router, prefix="/api")


@app.get("/", include_in_schema=False)
async def health() -> JSONResponse:
    return JSONResponse({"message": "It worked!!"})


async def init_models():
    async with async_engine.begin() as conn:
        # await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)


if __name__ == "__main__":
    import uvicorn
    # asyncio.run(init_models())
    uvicorn.run(app, host="0.0.0.0", port=8000)
