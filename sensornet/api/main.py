from fastapi import APIRouter

from .sensors.sensor import router as sensor_router
from .sensor_type.sensortype import router as type_router

router = APIRouter()
router.include_router(sensor_router)
router.include_router(type_router)