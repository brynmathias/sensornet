from pydantic import BaseModel
from sensornet.db_model.sensor import SensorTypeEnum, SourceTypeEnum
from uuid import UUID
from typing import List


class CreateSensorTypeMessage(BaseModel):
    type: SensorTypeEnum

class SensorTypeMessage(BaseModel):
    type: SensorTypeEnum
    id: UUID

class SensorTypeList(BaseModel):
    sensor_types: List[SensorTypeMessage]