from fastapi import APIRouter, Depends, Path, Request


from .schema import CreateSensorTypeMessage, SensorTypeMessage, SensorTypeList 
from .usecases import CreateSensorType, GetAllSensors
from uuid import UUID

router = APIRouter(prefix="/sensortype")


@router.post("", response_model=SensorTypeMessage)
async def create(
    request: Request,
    this_sensor: CreateSensorTypeMessage,
    use_case: CreateSensorType = Depends(CreateSensorType),
) -> SensorTypeMessage:
    return await use_case.execute(this_sensor)


@router.get("", response_model=SensorTypeList)
async def read_all(
    request: Request,
    use_case: GetAllSensors = Depends(GetAllSensors),
) -> SensorTypeList:
    return SensorTypeList(
        sensor_types=[SensorTypeMessage(id=sensor.id, type=sensor.type) async for sensor in use_case.execute()]
    )


# @router.get("/{uuid}", response_model=SensorType)
# async def read(
#     request: Request,
#     uuid: UUID = Path(..., description=""),
#     use_case: GetSensor = Depends(GetSensor),
# ) -> SensorInstance:
#     return await use_case.execute(uuid)
