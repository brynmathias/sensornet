from typing import Annotated, AsyncIterator

from fastapi import Depends, HTTPException
from sqlalchemy.ext.asyncio import async_sessionmaker

from sensornet.api.db import get_session
from sensornet.api.sensor_type.schema import CreateSensorTypeMessage, SensorTypeMessage, SensorTypeList
from sensornet.db_model.sensor import SensorType
from uuid import UUID
AsyncSession = Annotated[async_sessionmaker, Depends(get_session)]


class CreateSensorType:
    def __init__(self, session: AsyncSession) -> None:
        self.async_session = session

    async def execute(self, this_type: CreateSensorTypeMessage) -> SensorType:
        async with self.async_session.begin() as session:
            st = await SensorType.create(session, this_type.type)
            return SensorTypeMessage(id=st.id, type=st.type)


class GetAllSensors:
    def __init__(self, session: AsyncSession) -> None:
        self.async_session = session

    async def execute(self) -> AsyncIterator[SensorType]:
        async with self.async_session() as session:
            async for st in SensorType.read_all(session):
                yield SensorType(id=st.id, type=st.type)



class DeleteSensor:
    def __init__(self, session: AsyncSession) -> None:
        self.async_session = session

    async def execute(self, id: UUID) -> None:
        async with self.async_session.begin() as session:
            st = await SensorType.read_by_id(session, id)
            if not st:
                return
            await SensorType.delete(session, st)
