from typing import Annotated, AsyncIterator

from fastapi import Depends, HTTPException
from sqlalchemy.ext.asyncio import async_sessionmaker

from sensornet.api.db import get_session
from .schema import SensorInstance, CreateSensorMessage
from sensornet.db_model.sensor import DeployedSensor
from uuid import UUID
import logging

logger = logging.getLogger(__name__)

AsyncSession = Annotated[async_sessionmaker, Depends(get_session)]


class CreateSensor:
    def __init__(self, session: AsyncSession) -> None:
        self.async_session = session

    async def execute(self, this_sensor: CreateSensorMessage) -> SensorInstance:
        async with self.async_session.begin() as session:
            ds = await DeployedSensor.create(
                session,
                this_sensor.sensor_type,
                this_sensor.storage_path,
                this_sensor.source_fqdn,
                this_sensor.source_type,
            )
            ret = SensorInstance(
                uuid=ds.id,
                type=ds.sensor_type.type,
                storage_path=ds.storage_location,
                source_fqdn=ds.source_fqdn,
                source_type=ds.source_type,
            )
            logger.info(f"Created Sensor instance {ret=}")
            return ret 

class GetAllSensors:
    def __init__(self, session: AsyncSession) -> None:
        self.async_session = session

    async def execute(self) -> AsyncIterator[SensorInstance]:
        async with self.async_session() as session:
            async for ds in DeployedSensor.read_all(session):
                yield SensorInstance(
                    uuid=ds.id,
                    type=ds.sensor_type.type,
                    storage_path=ds.storage_location,
                    source_fqdn=ds.source_fqdn,
                    source_type=ds.source_type,
                )


class GetSensor:
    def __init__(self, session: AsyncSession) -> None:
        self.async_session = session

    async def execute(self, uuid: UUID) -> DeployedSensor:
        async with self.async_session() as session:
            ds = await DeployedSensor.read_by_id(session, uuid)
            if not ds:
                raise HTTPException(status_code=404)
            return SensorInstance(
                uuid=ds.id,
                storage_path=ds.storage_location,
                source_fqdn=ds.source_fqdn,
                source_type=ds.source_type,
            )


# class UpdateSensor:
#     def __init__(self, session: AsyncSession) -> None:
#         self.async_session = session

#     async def execute(self, note_id: int, notebook_id: int, title: str, content: str) -> NoteSchema:
#         async with self.async_session.begin() as session:
#             note = await Note.read_by_id(session, note_id)
#             if not note:
#                 raise HTTPException(status_code=404)

#             if note.notebook_id != notebook_id:
#                 notebook = await Notebook.read_by_id(session, notebook_id)
#                 if not notebook:
#                     raise HTTPException(status_code=404)
#                 notebook_id_ = notebook.id
#             else:
#                 notebook_id_ = note.notebook_id

#             await note.update(session, notebook_id_, title, content)
#             await session.refresh(note)
#             return NoteSchema.from_orm(note)


# class DeleteSensor:
#     def __init__(self, session: AsyncSession) -> None:
#         self.async_session = session

#     async def execute(self, note_id: int) -> None:
#         async with self.async_session.begin() as session:
#             note = await Note.read_by_id(session, note_id)
#             if not note:
#                 return
#             await Note.delete(session, note)
