from fastapi import APIRouter, Depends, Path, Request


from .schema import CreateSensorMessage, SensorInstance, ReadAllSensorResponse
from .usecases import CreateSensor, GetAllSensors, GetSensor
from uuid import UUID

router = APIRouter(prefix="/sensor")


@router.post("", response_model=SensorInstance)
async def create(
    request: Request,
    this_sensor: CreateSensorMessage,
    use_case: CreateSensor = Depends(CreateSensor),
) -> SensorInstance:
    return await use_case.execute(this_sensor)


@router.get("", response_model=ReadAllSensorResponse)
async def read_all(
    request: Request,
    use_case: GetAllSensors = Depends(GetAllSensors),
) -> ReadAllSensorResponse:
    return ReadAllSensorResponse(
        Sensors=[sensor async for sensor in use_case.execute()]
    )


@router.get("/{uuid}", response_model=SensorInstance)
async def read(
    request: Request,
    uuid: UUID = Path(..., description=""),
    use_case: GetSensor = Depends(GetSensor),
) -> SensorInstance:
    return await use_case.execute(uuid)
