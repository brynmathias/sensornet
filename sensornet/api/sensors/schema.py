from pydantic import BaseModel
from sensornet.db_model.sensor import SensorTypeEnum, SourceTypeEnum
from uuid import UUID
from typing import Optional, List


class CreateSensorMessage(BaseModel):
    sensor_type: SensorTypeEnum
    storage_path: str
    geo_lat_lon: List[int]
    source_fqdn: str
    source_type: SourceTypeEnum


class SensorInstance(BaseModel):
    uuid: UUID
    type: SensorTypeEnum
    storage_path: str
    geo_lat_lon: Optional[List[int]]
    source_fqdn: str
    source_type: SourceTypeEnum


class ReadAllSensorResponse(BaseModel):
    Sensors: list[SensorInstance]
