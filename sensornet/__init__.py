import dynaconf

settings = dynaconf.Dynaconf(
    envvar_prefix="SENSORNET",
    dotenv_path=".env",
    load_dotenv=True,
)
