from .base import Base
from sqlalchemy import String, Column
from sqlalchemy.dialects.postgresql import UUID, ENUM
from uuid import uuid4
from geoalchemy2 import Geometry
from sqlalchemy.ext.asyncio import AsyncSession
from uuid import UUID as UUIDT

from sqlalchemy.orm import Mapped, joinedload, mapped_column, relationship


from typing import List, Optional

from sqlalchemy import ForeignKey
from sqlalchemy import Integer, select
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import relationship
from enum import Enum, unique


@unique
class SensorTypeEnum(Enum):
    PiCamera = "PiCamera"
    ESP32 = "ESP32"


@unique
class SourceTypeEnum(Enum):
    RTSP = "rtsp"


class SensorType(Base):
    __tablename__ = "sensor_type"
    id: Mapped[uuid4] = mapped_column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    type: Mapped[SensorTypeEnum]
    instances: Mapped[List["DeployedSensor"]] = relationship(
        back_populates="sensor_type"
    )
    # details: Mapped[dict]

    @classmethod
    async def get_instance_by_type(
        cls, session: AsyncSession, sensor_type: SensorTypeEnum
    ) -> UUIDT:
        stmt = select(cls).where(cls.type == sensor_type)
        return await session.scalar(stmt)

    @classmethod
    async def create(cls, session: AsyncSession, type: str):
        this_instance = SensorType(type=type)
        session.add(this_instance)
        await session.flush()

        new = await cls.read_by_id(session, this_instance.id)
        if not new:
            raise RuntimeError()
        return new

    @classmethod
    async def read_all(cls, session: AsyncSession):
        stmt = select(cls)
        stream = await session.stream_scalars(stmt.order_by(cls.id))
        async for row in stream:
            yield row

    @classmethod
    async def read_by_id(cls, session: AsyncSession, id: UUIDT):
        stmt = select(cls).where(cls.id == id)
        return await session.scalar(stmt.order_by(cls.id))

class DeployedSensor(Base):
    __tablename__ = "deployed_sensor"
    id: Mapped[uuid4] = mapped_column(
        UUID(as_uuid=True), primary_key=True, default=uuid4
    )
    storage_location: Mapped[str] = mapped_column(nullable=False)
    # geographic_location: Mapped["Geometry"] = mapped_column(Geometry(geometry_type="POINT", srid=4326), index=True)
    # coverage_region: Mapped["Geometry"] = mapped_column(Geometry(geometry_type="POLYGON", srid=4326), index=True)# = Column(Geometry(geometry_type="POLYGON", srid=4326), index=True)
    type_id: Mapped[uuid4] = mapped_column(ForeignKey("sensor_type.id"))
    sensor_type: Mapped["SensorType"] = relationship(back_populates="instances")
    source_fqdn: Mapped[str] = mapped_column(nullable=False)
    source_type: Mapped["SourceTypeEnum"] = mapped_column(nullable=False)

    @classmethod
    async def create(
        cls,
        session: AsyncSession,
        sensor_type: SensorTypeEnum,
        storage_location: str,
        source_fqdn: str,
        source_type: SourceTypeEnum,
    ):
        sensor_type = await SensorType.get_instance_by_type(
            session, SensorTypeEnum(sensor_type)
        )
        sensor = DeployedSensor(
            storage_location=storage_location,
            sensor_type=sensor_type,
            source_fqdn=source_fqdn,
            source_type=source_type,
        )
        session.add(sensor)
        await session.flush()

        # To fetch notebook
        new = await cls.read_by_id(session, sensor.id)
        if not new:
            raise RuntimeError()
        return new


    @classmethod
    async def read_by_id(cls, session: AsyncSession, id: UUIDT):
        stmt = select(cls).where(cls.id == id)
        return await session.scalar(stmt.order_by(cls.id))


    @classmethod
    async def read_all(cls, session: AsyncSession):
        stmt = select(cls).options(joinedload(cls.sensor_type, innerjoin=True))
        stream = await session.stream_scalars(stmt.order_by(cls.id))
        async for row in stream:
            yield row